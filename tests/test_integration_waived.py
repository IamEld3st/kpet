# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests for "waived" property of test cases"""
from tests.test_integration import (IntegrationTests, kpet_run_generate,
                                    create_asset_files)


COMMON_YAML = """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree:
                        template: tree.txt.j2
                case:
                  host_type_regex: normal
                  location: somewhere
                  max_duration_seconds: 600
                  maintainers:
                    - maint1
"""

TREE_TXT_J2 = """
{%- for recipeset in RECIPESETS -%}
    {% for HOST in recipeset %}
        {% for test in HOST.tests %}
            {{ test.name }}: {{ test.waived }}
        {% endfor %}
    {% endfor %}
{%- endfor -%}
"""


class IntegrationWaivedTests(IntegrationTests):
    """Integration tests for "waived" property of test cases"""

    def test_defaults(self):
        """Check default "waived" values are correct"""
        assets = {
            "index.yaml": COMMON_YAML + """
                  cases:
                    A:
                      name: A
                    B:
                      name: B
                """,
            "tree.txt.j2": TREE_TXT_J2,
        }
        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "--no-lint",
            stdout_matching=r'.*(A|B): False.*(A|B): False.*'
        )

    def test_booleans_work(self):
        """Check boolean "waived" values work"""
        assets = {
            "index.yaml": COMMON_YAML + """
                  cases:
                    A:
                      name: A
                      waived: true
                    B:
                      name: B
                      waived: false
                """,
            "tree.txt.j2": TREE_TXT_J2,
        }
        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "--no-lint",
            stdout_matching=r'.*(A: True|B: False).*(A: True|B: False).*'
        )

    def test_patterns_work(self):
        """Check pattern "waived" values work"""
        assets = {
            "index.yaml": COMMON_YAML + """
                  cases:
                    A:
                      name: A
                      waived:
                        trees: tree
                    B:
                      name: B
                      waived:
                        not:
                          trees: tree
                """,
            "tree.txt.j2": TREE_TXT_J2,
        }
        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "--no-lint",
            stdout_matching=r'.*(A: True|B: False).*(A: True|B: False).*'
        )

    def test_branch_combination(self):
        """Check waived values are combined accross branches correctly"""
        assets = {
            "index.yaml": COMMON_YAML + """
                  waived: False
                  cases:
                    A:
                      name: A
                    B:
                      name: B
                """,
            "tree.txt.j2": TREE_TXT_J2,
        }
        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "--no-lint",
            stdout_matching=r'.*(A|B): False.*(A|B): False.*'
        )

        assets = {
            "index.yaml": COMMON_YAML + """
                  waived: True
                  cases:
                    A:
                      name: A
                    B:
                      name: B
                """,
            "tree.txt.j2": TREE_TXT_J2,
        }
        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "--no-lint",
            stdout_matching=r'.*(A|B): True.*(A|B): True.*'
        )

        assets = {
            "index.yaml": COMMON_YAML + """
                  waived: True
                  cases:
                    A:
                      name: A
                      waived: False
                    B:
                      name: B
                      waived: False
                """,
            "tree.txt.j2": TREE_TXT_J2,
        }
        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "--no-lint",
            stdout_matching=r'.*(A|B): True.*(A|B): True.*'
        )

        assets = {
            "index.yaml": COMMON_YAML + """
                  waived: False
                  cases:
                    A:
                      name: A
                      waived: True
                    B:
                      name: B
                      waived: False
                """,
            "tree.txt.j2": TREE_TXT_J2,
        }
        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "--no-lint",
            stdout_matching=r'.*(A: True|B: False).*(A: True|B: False).*'
        )

        assets = {
            "index.yaml": COMMON_YAML + """
                  waived: False
                  cases:
                    A:
                      name: A
                      waived: True
                      cases:
                        a:
                          name: a
                          waived: False
                    B:
                      name: B
                      waived: False
                      cases:
                        b:
                          name: b
                          waived: True
                """,
            "tree.txt.j2": TREE_TXT_J2,
        }
        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path, "--no-lint",
            stdout_matching=r'.*(A - a|B - b): True.*(A - a|B - b): True.*'
        )
