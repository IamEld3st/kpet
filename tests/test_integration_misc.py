# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration miscellaneous tests"""
from tests.test_integration import (IntegrationTests, kpet_run_generate,
                                    kpet_run_test_list,
                                    kpet_with_db, COMMONTREE_XML,
                                    INDEX_BASE_YAML, create_asset_files)


class IntegrationMiscTests(IntegrationTests):
    """Miscellaneous integration tests"""

    def test_empty_tree_list(self):
        """Test tree listing with empty database"""
        assets = {
            "index.yaml": """
                # Empty but valid database
                {}
            """,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(kpet_with_db, assets_path,
                                "-d", "tree", "list")

    def test_empty_run_generate(self):
        """Test run generation with empty database"""
        assets = {
            "index.yaml": """
                # Empty but valid database
                {}
            """,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(
            kpet_run_generate, assets_path,
            status=1,
            stderr_matching=r'.*Tree "tree" not found.*')

    def test_empty_run_test_list(self):
        """Test run test listing with empty database"""
        assets = {
            "index.yaml": """
                # Empty but valid database
                {}
            """,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(
            kpet_run_test_list, assets_path,
            status=1,
            stderr_matching=r'.*Tree "tree" not found.*')

    def test_minimal_run_generate(self):
        """Test run generation with minimal database"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                    panicky:
                        ignore_panic: true
                    multihost_1: {}
                recipesets:
                    rcs1:
                      - normal
                      - panicky
                    rcs2:
                      - multihost_1
                      - multihost_2

                # Minimal, but fully-functional database
                arches:
                    - arch
                trees:
                    tree:
                        template: tree.xml
            """,
            "tree.xml": COMMONTREE_XML,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(kpet_run_generate, assets_path,
                                stdout_matching=r'.*<job>\s*</job>.*')

    def test_minimal_run_test_list(self):
        """Test run test listing with minimal database"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                    panicky:
                        ignore_panic: true
                    multihost_1: {}
                recipesets:
                    rcs1:
                      - normal
                      - panicky
                    rcs2:
                      - multihost_1
                      - multihost_2

                # Minimal, but fully-functional database
                arches:
                    - arch
                trees:
                    tree:
                        template: tree.xml
            """,
            "tree.xml": COMMONTREE_XML,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(kpet_run_test_list, assets_path)

    def test_missing_tree_template_run_generate(self):
        """Test run generation with a missing tree template"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                    panicky:
                        ignore_panic: true
                    multihost_1: {}
                recipesets:
                    rcs1:
                      - normal
                      - panicky
                    rcs2:
                      - multihost_1
                      - multihost_2

                arches:
                    - arch
                trees:
                    missing_template:
                        template: missing_template.xml
            """,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(kpet_run_generate,
                                assets_path,
                                "-t", "missing_template",
                                status=1,
                                stderr_matching=r'.*TemplateNotFound.*')

    def test_missing_case_file_run_generate(self):
        """Test run generation with a missing case file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                trees:
                    tree:
                        template: tree.xml
                case: missing.yaml
            """,
            "tree.xml": """
                <job/>
            """,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(kpet_run_generate,
                                assets_path,
                                status=1,
                                stderr_matching=r'.*missing.yaml.*')

    def test_invalid_top_yaml_tree_list(self):
        """Test tree listing with invalid YAML in the top database file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                tree: {
            """,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(kpet_with_db, assets_path,
                                "--debug", "tree", "list",
                                status=1,
                                stderr_matching=r'.*yaml.parser.ParserError.*')

    def test_invalid_case_yaml_tree_list(self):
        """Test tree listing with invalid YAML in a case file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                case: case.yaml
            """,
            "case.yaml": """
                {
                maintainers:
            """,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(kpet_with_db, assets_path,
                                "--debug", "tree", "list",
                                status=1,
                                stderr_matching=r'.*yaml.parser.ParserError.*')

    def test_invalid_top_data_tree_list(self):
        """Test tree listing with invalid data in the top database file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                trees: {}
                unknown_node: True
            """,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(kpet_with_db, assets_path,
                                "tree", "list",
                                status=1,
                                stderr_matching=r'.*Invalid database.*')

    def test_invalid_case_data_tree_list(self):
        """Test tree listing with invalid data in a case file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                case: case.yaml
            """,
            "case.yaml": """
                name: "Case data with unknown nodes"
                foobar: True
                location: somewhere
                maintainers:
                  - maint1
            """,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(kpet_with_db, assets_path,
                                "tree", "list",
                                status=1,
                                stderr_matching=r'.*Invalid test case.*')

    def test_empty_abstract_case_run_generate(self):
        """Test run generation with an empty abstract case"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree:
                        template: tree.xml
                case:
                  host_type_regex: ^normal
                  cases: {}
            """,
            "tree.xml": COMMONTREE_XML,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(kpet_run_generate, assets_path,
                                stdout_matching=r'.*<job>\s*</job>.*')

    def test_empty_case_no_patterns_run_generate(self):
        """Test run generation with an empty test case without patterns"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: A
                      location: somewhere
                      maintainers:
                        - maint1
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
            """,
            "tree.xml": COMMONTREE_XML,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(
            kpet_run_generate, assets_path,
            stdout_matching=r'.*<job>\s*HOST\s*A - a\s*</job>.*')

    def test_empty_case_with_a_pattern_run_generate(self):
        """Test run generation with an empty test case with a pattern"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: A
                      location: somewhere
                      maintainers:
                        - maint1
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
            """,
            "tree.xml": COMMONTREE_XML,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(
            kpet_run_generate, assets_path,
            stdout_matching=r'.*<job>\s*HOST\s*A - a\s*</job>.*')

    def test_preboot_tasks_are_added(self):
        """Test preboot tasks are added"""
        assets = {
            "index.yaml": """
                host_types:
                    normal:
                        preboot_tasks: preboot_tasks.xml
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree:
                        template: tree.xml
                case:
                    maintainers: ["maint1"]
                    max_duration_seconds: 600
                    host_type_regex: ^normal
            """,
            "tree.xml": COMMONTREE_XML,
            "preboot_tasks.xml": "<task>Some preboot task</task>"
        }

        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path,
            stdout_matching=r'.*<task>\s*Some preboot task\s*</task>.*')

    def test_cases_expose_their_name(self):
        """Test case's "name" field should be exposed to Beaker templates"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: case A
                      location: somewhere
                      cases:
                        a:
                          name: case a
                          max_duration_seconds: 600
                          maintainers: ["maint1"]
            """,
            "tree.xml": """
            <job>
              {% for recipeset in RECIPESETS %}
                {% for HOST in recipeset %}
                  {% for test in HOST.tests %}
                    {{ test.name }}
                  {% endfor %}
                {% endfor %}
              {% endfor %}
            </job>
            """,
        }
        assets_path = create_asset_files(self.test_dir, assets)
        self.assertKpetProduces(
            kpet_run_generate, assets_path,
            stdout_matching=r'.*<job>\s*case A - case a\s*</job>.*')

    def test_cases_expose_their_maintainers(self):
        """Test cases' "maintainers" field should be exposed to templates"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      maintainers:
                          - Some Maintainer <someone@maintainers.org>
            """,
            "tree.xml": """
            <job>
              {% for recipeset in RECIPESETS %}
                {% for HOST in recipeset %}
                  {% for test in HOST.tests %}
                    {{ test.name }}
                    Maintainers:
                    {% for maintainer in test.maintainers %}
                      # {{ maintainer | e }}
                    {% endfor %}
                  {% endfor %}
                {% endfor %}
              {% endfor %}
            </job>
            """,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        self.assertKpetProduces(
            kpet_run_generate, assets_path,
            stdout_matching=r'.*<job>.*case1.*'
            r'# Some Maintainer &lt;someone@maintainers.org&gt;.*</job>.*')

    def test_tests_argument(self):
        """Test matching with --tests argument"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: A
                      location: somewhere
                      maintainers:
                        - maint1
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                    B:
                      name: B
                      location: somewhere
                      maintainers:
                        - maint1
                      cases:
                        b:
                          name: b
                          max_duration_seconds: 600
                        b2:
                          name: b2
                          max_duration_seconds: 600
            """,
            "tree.xml": COMMONTREE_XML,
        }

        assets_path = create_asset_files(self.test_dir, assets)

        test_name_re_output_re_map = {"B - b": r"HOST\s*B - b",
                                      "B.*": r"HOST\s*B - b\s*B - b2",
                                      "B": ""}
        for test_name_re in test_name_re_output_re_map:
            with self.subTest(test_name_re=test_name_re):
                self.assertKpetProduces(
                    kpet_run_generate, assets_path, "--tests", test_name_re,
                    stdout_matching=fr'.*'
                    fr'{test_name_re_output_re_map[test_name_re]}\s*</job>.*')
