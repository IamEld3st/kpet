# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""KPET data"""

import os
import re
from functools import reduce
from kpet import misc
from kpet.schema import Invalid, Struct, Choice, \
    List, Dict, String, Regex, ScopedYAMLFile, YAMLFile, Class, Boolean, \
    Int, Null, RE, Reduction

# pylint: disable=access-member-before-definition, no-member


# Schema for universal IDs
UNIVERSAL_ID_SCHEMA = String(pattern="[.a-zA-Z0-9_-]*")


class Object:   # pylint: disable=too-few-public-methods
    """An abstract data object"""
    def __init__(self, name, schema, data):
        """
        Initialize an abstract data object with a schema validating and
        resolving a supplied data.

        Args:
            name:   Name of the data instance to use in error messages.
            schema: The schema of the data, must recognize to a Struct.
            data:   The object data to be validated against and resolved with
                    the schema.
        """
        # Validate and resolve the data
        try:
            data = schema.resolve(data)
        except Invalid as exc:
            raise Invalid("Invalid {}".format(name)) from exc

        # Recognize the schema
        schema = schema.recognize()
        assert isinstance(schema, Struct)
        try:
            schema.validate(data)
        except Invalid as exc:
            raise Exception("Resolved {} is invalid:\n{}".format(name, exc)
                            ) from exc

        # Assign members
        for member_name in schema.required.keys():
            setattr(self, member_name, data[member_name])
        for member_name in schema.optional.keys():
            setattr(self, member_name, data.get(member_name, None))


class Target:  # pylint: disable=too-few-public-methods, too-many-arguments
    """
    Execution target which case patterns match against.

    A target is describing a single execution of a test.

    A target has a fixed collection of parameters, each of which can be
    assigned a target set.

    A target set is either:

        - a set of strings,
        - Target.ALL, meaning a set containing all possible strings, or
        - Target.UNKNOWN, meaning any possible set of strings.
    """

    # Empty target set
    NONE = set()
    # Complete target set
    ALL = ()
    # Unknown target set
    UNKNOWN = None

    @staticmethod
    def set_is_valid(target_set):
        """
        Check if a target set is valid.

        Args:
            target_set: The target set to check.

        Returns:
            True if the target set is valid, false otherwise.
        """
        return target_set == Target.UNKNOWN or \
            target_set == Target.ALL or \
            (isinstance(target_set, set) and
             all(isinstance(x, str) for x in target_set))

    @staticmethod
    def set_is_unit(target_set):
        """
        Check if a target set is a unit set (has only one member).

        Args:
            target_set: The target set to check.

        Returns:
            True if the target set is a unit set, false otherwise.
        """
        return isinstance(target_set, set) and len(target_set) == 1

    def __init__(self, trees=None, arches=None, components=None, sources=None):
        """
        Initialize a target.

        Args:
            trees:          A target set of names of the kernel trees we're
                            executing against.
                            Can only be a unit set or UNKNOWN.
            arches:         A target set of names of the architectures we're
                            executing on. Can only be a unit set or UNKNOWN.
            components:     A target set of names of extra components included
                            into the tested kernel build.
            sources:        A target set of paths to the source files to cover
                            with tests.
        """
        assert Target.set_is_valid(trees)
        assert Target.set_is_unit(trees) or trees is Target.UNKNOWN
        assert Target.set_is_valid(arches)
        assert Target.set_is_unit(arches) or arches is Target.UNKNOWN
        assert Target.set_is_valid(components)
        assert Target.set_is_valid(sources)

        self.trees = trees
        self.arches = arches
        self.components = components
        self.sources = sources

    def __repr__(self):
        attrs = []
        for attr, value in self.__dict__.items():
            value = getattr(self, attr)
            if value is Target.NONE:
                value_repr = "NONE"
            elif value is Target.UNKNOWN:
                value_repr = "UNKNOWN"
            elif value is Target.ALL:
                value_repr = "ALL"
            else:
                value_repr = repr(value)
            attrs.append(attr + "=" + value_repr)
        return "<" + ", ".join(attrs) + ">"


class Pattern(Object):  # pylint: disable=too-few-public-methods
    """Execution target pattern"""

    # Target field qualifiers
    qualifiers = {"trees", "arches", "components", "sources"}

    """An execution target pattern"""
    def __init__(self, data):
        """
        Initialize an execution pattern.

        Args:
            data:       Pattern data.
        """
        class NonRecursiveChoice(Choice):
            """Choice schema preventing recursive recognition"""
            def __init__(self, *args):
                super().__init__(*args)
                self.recognizing = False

            def recognize(self):
                if self.recognizing:
                    recognized = self
                else:
                    self.recognizing = True
                    recognized = super().recognize()
                    self.recognizing = False
                return recognized

        class OpsOrValues(NonRecursiveChoice):
            """Pattern operations or values"""
            def __init__(self):
                super().__init__(
                    Null(),
                    Regex(),
                    List(self),
                    Struct(optional={k: self for k in {"not", "and", "or"}})
                )

        ops_or_values_schema = OpsOrValues()

        class OpsOrQualifiers(NonRecursiveChoice):
            """Pattern operations or qualifiers"""
            def __init__(self):
                fields = {}
                fields.update({k: self for k in {"not", "and", "or"}})
                fields.update({k: ops_or_values_schema
                               for k in Pattern.qualifiers})
                super().__init__(
                    List(self),
                    Struct(optional=fields)
                )

        try:
            self.data = Choice(OpsOrQualifiers(), Boolean()).resolve(data)
        except Invalid as exc:
            raise Invalid("Invalid pattern") from exc

    # Documentation overhead for multiple functions would be too big, and
    # spread-out logic too hard to grasp.
    # pylint: disable=too-many-branches
    def __node_matches(self, target, and_op, node, qualifier):
        """
        Check if a pattern node matches a target.

        Args:
            target:     The target (an instance of Target) to match.
            and_op:     True if the node items should be "and'ed" together,
                        False if "or'ed".
            node:       The pattern node matching against the target.
                        Either None, a regex, a dictionary or a list.
            qualifier:  Qualifier (name of the target parameter being
                        matched), if already encountered, None if not.
                        Cannot be None if node is a None or a regex.

        Returns:
            True if the node matched, False if not,
            and None if the result could be any.
        """
        assert isinstance(target, Target)
        assert and_op in (False, True)
        assert qualifier is None or qualifier in self.qualifiers

        # The trinary logic truth table for all possible operations/operands.
        # See https://en.wikipedia.org/wiki/Three-valued_logic
        # Indexed as [and_op][x][y].
        truth_table = {
            # OR
            False: {
                False:  {False: False,  None: None,     True: True},
                None:   {False: None,   None: None,     True: True},
                True:   {False: True,   None: True,     True: True},
            },
            # AND
            True: {
                False:  {False: False,  None: False,    True: False},
                None:   {False: False,  None: None,     True: None},
                True:   {False: False,  None: None,     True: True},
            },
        }

        def sub_op(result_x, result_y):
            """Combine two sub-results using the specified operation"""
            assert result_x in (False, None, True)
            assert result_y in (False, None, True)
            return truth_table[and_op][result_x][result_y]

        if isinstance(node, dict):
            sub_results = []
            for name, sub_node in node.items():
                assert qualifier is None or name not in self.qualifiers, \
                       "Qualifier is already specified"
                sub_result = self.__node_matches(
                    target, (name != "or"), sub_node,
                    name if name in self.qualifiers else qualifier)
                if sub_result is not None and name == "not":
                    sub_result = not sub_result
                sub_results.append(sub_result)
            result = reduce(sub_op, sub_results) if sub_results else and_op
        elif isinstance(node, list):
            sub_results = [
                self.__node_matches(target, True, sub_node, qualifier)
                for sub_node in node
            ]
            result = reduce(sub_op, sub_results) if sub_results else and_op
        elif isinstance(node, RE):
            assert qualifier is not None, "Qualifier not specified"
            param = getattr(target, qualifier)
            if param == Target.ALL:
                result = True
            elif param == Target.UNKNOWN:
                result = None
            else:
                for value in param:
                    if node.fullmatch(value):
                        result = True
                        break
                else:
                    result = False
        elif node is None:
            assert qualifier is not None, "Qualifier not specified"
            param = getattr(target, qualifier)
            if param == Target.UNKNOWN:
                result = None
            else:
                result = (param == Target.ALL)
        else:
            assert False, "Unknown node type: " + type(node).__name__

        return result

    def matches(self, target):
        """
        Check if the pattern matches a target.

        Args:
            target: The target (an instance of Target) to match.

        Returns:
            True if the pattern matches the target, False if not.
        """
        assert isinstance(target, Target)
        if isinstance(self.data, bool):
            return self.data
        node_matches = self.__node_matches(target, True, self.data, None)
        return node_matches is None or node_matches


class Case(Object):     # pylint: disable=too-few-public-methods
    """Universal test case"""

    def __init__(self, data):
        sets_schema = Reduction(Regex(), lambda x: [x], List(Regex()))

        super().__init__(
            "test case",
            Struct(
                required=dict(),
                optional=dict(
                    name=String(),
                    universal_id=UNIVERSAL_ID_SCHEMA,
                    origin=String(),
                    location=String(),
                    max_duration_seconds=Int(),
                    host_type_regex=Regex(),
                    host_requires=String(),
                    partitions=String(),
                    kickstart=String(),
                    sets=sets_schema,
                    enabled=Class(Pattern),
                    waived=Class(Pattern),
                    role=String(),
                    environment=Dict(String()),
                    maintainers=List(String()),
                    cases=Dict(key_schema=String(pattern="[a-zA-Z0-9_-]*"),
                               value_schema=Choice(YAMLFile(Class(Case)),
                                                   Class(Case)))
                )
            ),
            data
        )
        if self.enabled is None:
            self.enabled = Pattern(True)
        if self.waived is None:
            self.waived = Pattern(False)
        if self.environment is None:
            self.environment = {}
        if self.maintainers is None:
            self.maintainers = []

        self.id = None
        self.parent = None
        if self.cases is not None:
            for id, case in self.cases.items():
                case.id = id
                case.parent = self

    def is_enabled_for(self, target):
        """
        Check if the case is enabled for a target.

        Args:
            target: The target to check against.

        Returns:
            True if the case is enabled for the target, False otherwise.
        """
        return self.enabled.matches(target)

    def is_waived_for(self, target):
        """
        Check if the case is waived for a target.

        Args:
            target: The target to check against.

        Returns:
            True if the case is waived for the target, False otherwise.
        """
        return self.waived.matches(target)


class Test:
    # pylint: disable=too-few-public-methods, too-many-instance-attributes
    """A test run - an instance of a test case"""

    def __init__(self, case):
        """
        Initialize a test run as an instance of a test case.

        Args:
            case:           The test case to instantiate.
        """
        assert isinstance(case, Case)

        self.case = case
        self.name = " - ".join(
            reversed(tuple(misc.attr_parentage(case, "name")))
        )
        self.universal_id = next(
            misc.attr_parentage(case, "universal_id"), None
        )
        self.origin = next(misc.attr_parentage(case, "origin"), None)
        self.location = next(misc.attr_parentage(case, "location"), None)
        self.max_duration_seconds = next(
            misc.attr_parentage(case, "max_duration_seconds"), None
        )
        self.host_type_regex = next(
            misc.attr_parentage(case, "host_type_regex"), None
        )
        self.role = next(misc.attr_parentage(case, "role"), "STANDALONE")
        self.environment = reduce(
            lambda x, y: {**y, **x}, misc.attr_parentage(case, "environment"),
            {}
        )
        self.maintainers = reduce(
            lambda x, y: y + x, misc.attr_parentage(case, "maintainers"), []
        )
        self.sets = case.sets or set()

    def is_enabled_for(self, target):
        """
        Check if the test is enabled for a target.

        Args:
            target: The target to check against.

        Returns:
            True if the test is enabled for the target, False otherwise.
        """
        return all(enabled.matches(target)
                   for enabled in misc.attr_parentage(self.case, "enabled"))

    def is_waived_for(self, target):
        """
        Check if the test is waived for a target.

        Args:
            target: The target to check against.

        Returns:
            True if the test is waived for the target, False otherwise.
        """
        return any(waived.matches(target)
                   for waived in misc.attr_parentage(self.case, "waived"))


class HostType(Object):     # pylint: disable=too-few-public-methods
    """Host type"""

    def __init__(self, data):
        """
        Initialize a host type.
        """

        super().__init__(
            "host type",
            Struct(optional=dict(
                ignore_panic=Boolean(),
                host_requires=String(),
                hostname=String(),
                partitions=String(),
                kickstart=String(),
                preboot_tasks=String(),
                postboot_tasks=String(),
            )),
            data
        )


class Base(Object):     # pylint: disable=too-few-public-methods
    """Database"""

    @staticmethod
    def is_dir_valid(dir_path):
        """
        Check if a directory is a valid database.

        Args:
            dir_path:   Path to the directory to check.

        Returns:
            True if the directory is a valid database directory,
            False otherwise.
        """
        return os.path.isfile(dir_path + "/index.yaml")

    # pylint: disable=too-many-branches
    def case_resolve(self, case, sets):
        """
        Validate and resolve a case and its sub-cases.

        Args:
            case:   The case to resolve.
            sets:   A set of names of sets the case can belong to, or None,
                    if it isn't defined and self.sets should be used.
        """
        assert isinstance(case, Case)
        assert sets is None or \
            isinstance(sets, set) and \
            all(isinstance(set_name, str) for set_name in sets)

        if case.parent:
            if case.parent.parent:
                case.path = case.parent.path + "/" + case.id
            else:
                case.path = "/" + case.id
            case.ref = f"case {case.path}"
        else:
            case.path = "/"
            case.ref = "the root case"

        # Check host_type_regex matches something
        host_type_names = tuple(self.host_types or {})
        if case.host_type_regex is not None and \
           not any(case.host_type_regex.fullmatch(name)
                   for name in host_type_names):
            raise Invalid(f'Host type regex "{case.host_type_regex.pattern}" '
                          f'of {case.ref} does not match any of the '
                          f'available host type names: {host_type_names}')

        # Check case origin is valid
        if self.origins is None:
            if case.origin is not None:
                raise Invalid(
                    f'{case.ref.capitalize()} has origin specified, '
                    f'but available origins are not defined in '
                    f'the database.'
                )
        else:
            if case.origin is not None and \
               case.origin not in self.origins:
                raise Invalid(
                    f'{case.ref.capitalize()} has unknown origin '
                    f'specified: "{case.origin}".\n'
                    f'Expecting one of the following: '
                    f'{", ".join(self.origins.keys())}.'
                )

        # Resolve set regexes into set names
        if case.sets is None:
            case.sets = sets
        else:
            resolved_sets = set()
            for regex in case.sets:
                matches = set(
                    filter(regex.fullmatch,
                           set(self.sets) if sets is None else sets)
                )
                if not matches:
                    raise Invalid(f"{case.ref.capitalize()} set regex "
                                  f"\"{regex.pattern}\" doesn't match "
                                  f"any of the available sets: {sets}")
                resolved_sets |= matches
            case.sets = resolved_sets
            sets = resolved_sets

        # Resolve sub-cases if any
        for subcase in (case.cases or {}).values():
            self.case_resolve(subcase, sets)

    def case_render_tests(self, case, tests):
        """
        Render tests for a case and its children.

        Args:
            case:   The case to render the tests for.
            tests:  The dictionary to store rendered tests in.
        """
        assert isinstance(tests, dict)
        assert all(
            isinstance(name, str) and isinstance(test, Test)
            for name, test in tests.items()
        )
        assert isinstance(case, Case)

        # If this is a test case (a leaf node)
        if case.cases is None:
            # Create the test
            test = Test(case)
            # Check the test name is unique
            if test.name in tests:
                raise Invalid(f"Test for {case.ref} has a non-unique name: "
                              f"{test.name}")
            # Check the test has at least one maintainer
            if not test.maintainers:
                raise Invalid(f"Test \"{test.name}\" "
                              f"for {case.ref} has no maintainers")
            # Check the test has an origin, if needed
            if self.origins is not None and test.origin is None:
                raise Invalid(f"Test \"{test.name}\" "
                              f"for {case.ref} has no origin specified")
            # Add the test
            tests[test.name] = test
        # Else it's an abstract case
        else:
            # Render tests for sub-cases
            for subcase in case.cases.values():
                self.case_render_tests(subcase, tests)

    def convert_tree_arches(self):
        """
        Convert each tree's supported architecture specification from
        a list of regexes to a list of architecture names matching those
        regexes

        Returns:
            Raises a schema.Invalid exception when finding an invalid regex
        """

        wildcard = [re.compile(".*")]

        for name, value in self.trees.items():
            tree_arches = set()
            for arch_regex in value.get("arches", wildcard):
                regex_arches = set(filter(arch_regex.fullmatch, self.arches))
                if regex_arches == set():
                    error = ("One of 'trees: arches:' regexes\n" +
                             "doesn't match any of the available arches\n" +
                             "The regex: '{0}'\n" +
                             "The avaliable arches: {1}")
                    raise Invalid(error.format(arch_regex.pattern,
                                               self.arches))
                tree_arches |= regex_arches

            self.trees[name]["arches"] = list(tree_arches)

    def __init__(self, dir_path):
        """
        Initialize a database object.
        """
        assert self.is_dir_valid(dir_path)

        arches_schema = Reduction(Regex(), lambda x: [x], List(Regex()))

        super().__init__(
            "database",
            ScopedYAMLFile(
                Struct(optional=dict(
                    trees=Dict(
                        Struct(required=dict(template=String()),
                               optional=dict(arches=arches_schema))
                    ),
                    arches=List(String()),
                    components=Dict(String()),
                    sets=Dict(String()),
                    host_types=Dict(Class(HostType)),
                    recipesets=Dict(List(String())),
                    variables=Dict(
                        Struct(required=dict(description=String()),
                               optional=dict(default=String()))
                    ),
                    origins=Dict(String()),
                    case=Choice(YAMLFile(Class(Case)),
                                Class(Case))
                ))
            ),
            dir_path + "/index.yaml"
        )

        self.dir_path = dir_path
        if self.trees is None:
            self.trees = {}
        if self.arches is None:
            self.arches = []
        if self.components is None:
            self.components = {}
        if self.sets is None:
            self.sets = {}
        if self.variables is None:
            self.variables = dict()
        self.convert_tree_arches()
        self.tests = {}
        if self.case is not None:
            self.case_resolve(self.case, None)
            self.case_render_tests(self.case, self.tests)


class TestView:     # pylint: disable=too-few-public-methods
    """
    A test view.
    """
    # make Test attributes accessible without having to use `.view_test`
    def __getattr__(self, attr):
        return getattr(self.view_test, attr)

    def __init__(self, test, targets):
        """
        Initialize a test view.

        Args:
            test:           The test to create a view of.
            targets:        A list of test execution targets (data.Target
                            objects) to check waived status against.
        """
        assert isinstance(test, Test)
        assert isinstance(targets, list) and \
            all(isinstance(t, Target) for t in targets)
        self.view_test = test
        self.waived = all(test.is_waived_for(t) for t in targets)


class View:     # pylint: disable=too-few-public-methods
    """A database view."""

    # make Base attributes accessible without having to use `.view_base`
    def __getattr__(self, attr):
        return getattr(self.view_base, attr)

    def __init__(self, database, targets, match_sets=None, test_regexes=None):
        """
        Initialize a database view.

        Args:
            database:       The database to get test data from.
            targets:        A list of test execution targets (data.Target
                            objects) to match/filter tests with.
                            Each target's tree, architecture, and components
                            must be present in the database.
            match_sets:     A function matching sets a test belongs to and
                            returning True, if it should be included in the
                            filtering, and False, if not. Must accept a set of
                            strings (names of sets the test belongs to) as the
                            only argument. Can be None to match any set
                            membership. Default is None.
            test_regexes:   A list of string regexes to match
                            test names against.
                            Tests that match any regex will be included in the
                            filtering. None if all tests should be included.
                            Default is None.
        """
        assert isinstance(database, Base)
        assert isinstance(targets, list) and \
            all(
                isinstance(t, Target) and
                (t.trees is Target.UNKNOWN or
                 t.trees <= set(database.trees)) and
                (t.arches is Target.UNKNOWN or
                 t.arches <= set(database.arches)) and
                (t.components is Target.UNKNOWN or t.components is t.ALL or
                 t.components <= set(database.components))
                for t in targets
            )
        assert match_sets is None or callable(match_sets)
        assert test_regexes is None or \
            (isinstance(test_regexes, list) and
             all(isinstance(regex, str) for regex in test_regexes))

        self.view_base = database
        self.view_targets = targets
        self.view_match_sets = match_sets or (lambda _: True)
        self.view_test_regexes = test_regexes
        self.view_tests = []
        # Filter tests matching the parameters
        for test in self.tests.values():
            if self.view_test_regexes is not None and \
               not any(re.fullmatch(regex, test.name)
                       for regex in self.view_test_regexes):
                continue
            if not self.view_match_sets(test.sets):
                continue
            if not any(test.is_enabled_for(target)
                       for target in self.view_targets):
                continue
            self.view_tests.append(TestView(test, targets))
