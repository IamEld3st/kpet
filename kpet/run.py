# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Execution of tests from the database"""

import jinja2
from lxml import etree
from kpet import data


class Host:
    # pylint: disable=too-few-public-methods, too-many-instance-attributes
    """A host running tests"""

    def __init__(self, type, tests):
        """
        Initialize a host run.

        Args:
            type:   Type of the host.
            tests:  A list of tests (kpet.data.TestView instances).
        """
        assert isinstance(type, data.HostType)
        assert isinstance(tests, list)
        assert all(isinstance(test, data.TestView) for test in tests)

        self.hostname = type.hostname
        self.ignore_panic = type.ignore_panic
        self.preboot_tasks = type.preboot_tasks
        self.postboot_tasks = type.postboot_tasks
        self.tests = tests

        # Collect all unique cases referenced by tests,
        # in per-test top-bottom series
        cases = []
        for test in tests:
            pos = len(cases)
            case = test.case
            while case is not None:
                if case not in cases:
                    cases.insert(pos, case)
                case = case.parent

        # Assemble host_requires, partitions and kickstart lists
        # Collect host parameters and create "suite" and "test" lists
        host_requires_list = [type.host_requires]
        partitions_list = [type.partitions]
        kickstart_list = [type.kickstart]
        for case in cases:
            host_requires_list.append(case.host_requires)
            partitions_list.append(case.partitions)
            kickstart_list.append(case.kickstart)

        # Remove undefined template paths
        self.host_requires_list = filter(lambda e: e is not None,
                                         host_requires_list)
        self.partitions_list = filter(lambda e: e is not None,
                                      partitions_list)
        self.kickstart_list = filter(lambda e: e is not None,
                                     kickstart_list)

        # Put waived tests at the end
        self.tests.sort(key=lambda t: t.waived)


class Scenario:     # pylint: disable=too-few-public-methods
    """A scenario for execution of tests from a database"""

    def __init__(self, dataview):
        """
        Initialize a test run scenario.

        Args:
            dataview:   The database view to create a scenario from.
                        The view must have exactly one target, which should
                        have both the tree and the architecture known.
        """
        assert isinstance(dataview, data.View)
        assert len(dataview.view_targets) == 1

        self.target = dataview.view_targets[0]

        assert self.target.trees is not data.Target.UNKNOWN
        assert self.target.arches is not data.Target.UNKNOWN

        self.dataview = dataview

        host_type_tests = {}
        for test in dataview.view_tests:
            # Assign the test to a host type
            for host_type_name in dataview.host_types:
                if test.host_type_regex and \
                   test.host_type_regex.fullmatch(host_type_name):
                    tests = host_type_tests.get(host_type_name, [])
                    tests.append(test)
                    host_type_tests[host_type_name] = tests
                    break
            else:
                raise Exception(
                    f"No host type found for test \"{test.name}\" "
                    f"based on {test.case.ref}"
                )

        # Distribute host types to recipesets
        self.recipesets = []
        for recipeset_host_type_names in dataview.recipesets.values():
            recipeset = []
            for recipeset_host_type_name in recipeset_host_type_names:
                for host_type_name, tests in host_type_tests.items():
                    if host_type_name == recipeset_host_type_name:
                        recipeset.append(Host(
                            dataview.host_types[host_type_name],
                            tests
                        ))
                        del host_type_tests[host_type_name]
                        break
            if recipeset:
                self.recipesets.append(recipeset)

    # pylint: disable=too-many-arguments
    def generate(self, description, kernel_location, lint, variables):
        """
        Generate Beaker XML which would execute tests in the scenario.
        The target supplied at creation must have exactly one tree and exactly
        one architecture for this to succeed.

        Args:
            description:        The run description string.
            kernel_location:    Kernel location string (a tarball or RPM URL).
            lint:               Lint and reformat the XML output, if True.
            variables:          A dictionary of extra template variables.
        Returns:
            The beaker XML string.
        """
        assert isinstance(description, str)
        assert isinstance(kernel_location, str)
        assert isinstance(lint, bool)
        assert isinstance(variables, dict)

        tree_name = list(self.target.trees)[0]
        arch_name = list(self.target.arches)[0]

        params = dict(
            DESCRIPTION=description,
            KURL=kernel_location,
            ARCH=arch_name,
            TREE=tree_name,
            RECIPESETS=self.recipesets,
            VARIABLES=variables,
        )

        jinja_env = jinja2.Environment(
            loader=jinja2.FileSystemLoader([self.dataview.dir_path]),
            trim_blocks=True,
            keep_trailing_newline=True,
            lstrip_blocks=True,
            autoescape=jinja2.select_autoescape(
                enabled_extensions=('xml'),
                default_for_string=True,
            ),
            undefined=jinja2.StrictUndefined,
        )
        template = jinja_env.get_template(
                        self.dataview.trees[tree_name]['template'])
        text = template.render(params)

        if lint:
            parser = etree.XMLParser(remove_blank_text=True)
            tree = etree.XML(text.encode("utf-8"), parser)
            text = etree.tostring(tree, encoding="utf-8",
                                  xml_declaration=True,
                                  pretty_print=True).decode("utf-8")
        return text
